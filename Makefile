all: ipPurge package

ipPurge: ipPurge.go
	GOOS=linux go build ipPurge.go

package: ipPurge
	zip ipPurge.zip ipPurge
