package main

import (
	"errors"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"log"
	"quint.as/awsutils"
	"quint.as/iptracker"
	"time"
)

// Handler is your Lambda function handler
func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	// stdout and stderr are sent to AWS CloudWatch Logs
	log.Printf("Debug: ++Handler()")
	defer log.Printf("Debug: --Handler()")

	log.Printf("Info: Processing Lambda request %s\n", request.RequestContext.RequestID)

	err := purgeDb()

	if err != nil {
		log.Printf("Error: failed to write item to DynamoDB: " + err.Error())
		return awsutils.GenerateProxyResponse("{\"error\": \"failed to purge records\"}", 500)
	}

	return awsutils.GenerateProxyResponse("{\"success\": true}", 200)
}

func purgeDb() error {
	log.Printf("Debug: ++purgeDb()")
	defer log.Printf("Debug: --purgeDb()")

	oneWeekAgo := getUTCTimeMinus1Week()

	result, err := scanForRecordsToPurge(oneWeekAgo)

	if err != nil {
		return errors.New("DB scan failed: " + err.Error())
	}

	numItems := len(result.Items)
	numItemsDeleted := 0

	log.Printf("Info: found %d items in scan", numItems)

	for _, i := range result.Items {
		item := iptracker.Record{}

		err = dynamodbattribute.UnmarshalMap(i, &item)

		if err != nil {
			log.Printf("Error: failed to unmarshal item: " + err.Error())
		}

		if item.Timestamp < oneWeekAgo && item.IsMostRecentRecord == false {
			log.Println("Info: Found item older than 1 week and not most recent record: " + item.Guid)

			err := iptracker.DeleteRecordWithGuid(item.Guid)

			if err != nil {
				log.Printf("Error: failed to delete item with guid " + item.Guid + ": " + err.Error())
			} else {
				numItemsDeleted++
				log.Printf("Info: successfully deleted item with guid " + item.Guid)
			}
		}
	}

	log.Printf("Info: deleted %d items", numItemsDeleted)

	return nil
}

func scanForRecordsToPurge(period string) (*dynamodb.ScanOutput, error) {
	log.Printf("Debug: ++scanForRecordsToPurge()")
	defer log.Printf("Debug: --scanForRecordsToPurge()")

	ses, err := session.NewSession()
	svc := dynamodb.New(ses)

	filt := expression.Name("timeStamp").LessThan(expression.Value(period))
	proj := expression.NamesList(expression.Name("guid"))

	expr, err := expression.NewBuilder().WithFilter(filt).WithProjection(proj).Build()

	if err != nil {
		return nil, errors.New("failed to build expression: " + err.Error())
	}

	params := &dynamodb.ScanInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
		ProjectionExpression:      expr.Projection(),
		TableName:                 aws.String("ip_tracker"),
	}

	result, err := svc.Scan(params)
	if err != nil {
		log.Printf("Error: failed to perform dynamodb scan: " + err.Error())
		return nil, errors.New("failed to perform dynamodb scan")
	}

	return result, nil
}

func getUTCTimeMinus1Week() string {
	log.Printf("Debug: ++getUTCTimeMinus1Week()")
	defer log.Printf("Debug: --getUTCTimeMinus1Week()")

	return time.Now().AddDate(0, 0, -7).String()
}

func main() {
	lambda.Start(Handler)
}
