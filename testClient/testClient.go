package main

import (
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/djhworld/go-lambda-invoke/golambdainvoke"
)

func main() {
	response, err := golambdainvoke.Run(golambdainvoke.Input{
		Port: 8002,
		Payload: events.APIGatewayProxyRequest{
			Body:                  "",
		},
	})

	if err != nil {
		fmt.Println("Error: " + err.Error())
	} else {
		fmt.Println(string(response))
	}
}
